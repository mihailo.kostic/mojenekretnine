package com.example.mojenekretnine.adapter;

import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.mojenekretnine.R;
import com.example.mojenekretnine.model.Nekretnina;
import com.squareup.picasso.Picasso;

import java.util.List;

public class RVAdapter extends RecyclerView.Adapter<RVAdapter.MyViewHolder> {

    public List<Nekretnina> listaNekretnina;
    public OnRVClickedListener listener;

    public interface OnRVClickedListener{
        void onRVClicked(Nekretnina nekretnina);
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView imageViewRV;
        TextView tvTitleRV;
        View view;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            view = itemView;
            imageViewRV = itemView.findViewById(R.id.imageViewRV);
            tvTitleRV = itemView.findViewById(R.id.tvTitleRV);
        }

        public void bind(final Nekretnina nekretnina, final OnRVClickedListener listener){

            tvTitleRV.setText(nekretnina.getNaziv());
            imageViewRV.setImageBitmap(BitmapFactory.decodeFile(nekretnina.getSlika()));
//            Picasso.get()
//                    .load("http://becomingminimalist.com/wp-content/uploads/2008/07/post-it-note.jpg?w=300")
//                    .resize(30, 30)
//                    .into(imageViewRV);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onRVClicked(nekretnina);
                }
            });
        }
    }

    public RVAdapter(List<Nekretnina> nekretninaList, OnRVClickedListener listener) {
        this.listaNekretnina = nekretninaList;
        this.listener = listener;
    }

    @NonNull
    @Override
    public RVAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.rv_single_item, viewGroup, false);
        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RVAdapter.MyViewHolder myViewHolder, int i) {
        myViewHolder.bind(listaNekretnina.get(i), listener);

        int currentPosition = i;
        final Nekretnina nekretnina = listaNekretnina.get(currentPosition);
        myViewHolder.imageViewRV.setImageBitmap(BitmapFactory.decodeFile(nekretnina.getSlika()));
    }

    @Override
    public int getItemCount() {
        return listaNekretnina.size();
    }
}
