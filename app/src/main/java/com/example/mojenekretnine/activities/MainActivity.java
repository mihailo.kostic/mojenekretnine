package com.example.mojenekretnine.activities;

import android.Manifest;
import android.app.Dialog;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.preference.PreferenceManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.example.mojenekretnine.AboutDijalog;
import com.example.mojenekretnine.App;
import com.example.mojenekretnine.DatabaseHelper;
import com.example.mojenekretnine.R;
import com.example.mojenekretnine.adapter.RVAdapter;
import com.example.mojenekretnine.model.Nekretnina;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.squareup.picasso.Picasso;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements RVAdapter.OnRVClickedListener {

    Toolbar toolbar;
    private SharedPreferences sharedPreferences;
    private List<String> drawerItems;
    private ActionBarDrawerToggle drawerToggle;
    DatabaseHelper databaseHelper;
    AlertDialog aboutDijalog;

    private List<Nekretnina> listaNekretnina;
    private RecyclerView recyclerView;
    private RVAdapter adapterRV;

    EditText etNaziv;
    EditText etOpis;
    EditText etAdresa;
    private EditText etBrojTelefona;
    private EditText etKvadratura;
    private EditText etBrojSoba;
    private EditText etCena;
    private ImageView imageViewNekretnina;
    private Button btnCancel;
    private Button btnSave;
    private Button btnDodajSliku;


    public static final int NOTIF_ID = 15;
    public static String TOAST_SETTINGS = "toast_settings_cb";
    public static String NOTIF_SETTINGS = "notif_settings_cb";

    private static final int SELECT_PICTURE = 0;
    public static String picturePath;
//    private ImageView imageViewRV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setupToolbar();
        fillDrawerWithItems();
        setupDrawer();
        showList();

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
    }


    private void setupToolbar() {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);
            actionBar.setHomeAsUpIndicator(R.drawable.ic_action_home);
            actionBar.show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_add:
                dodajNekretninu();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void fillDrawerWithItems() {
        drawerItems = new ArrayList<>();
        drawerItems.add("Sve nekretnine");
        drawerItems.add("Podesavanja");
        drawerItems.add("About");
    }

    private void setupDrawer() {
        final LinearLayout linearZaDrawer = findViewById(R.id.linear_za_drawerList);
        final DrawerLayout masterDrawerLayout = findViewById(R.id.masterDrawerLayout);
        ListView lvDrawer = findViewById(R.id.lvDrawer);
        lvDrawer.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, drawerItems));
        lvDrawer.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String naslov = "nepoznato";
                switch (position) {
                    case 0:
                        naslov = "Sve nekretnine";
                        showList();
                        break;
                    case 1:
                        naslov = "Podesavanja";
                        showSettings();
                        break;
                    case 2:
                        naslov = "About";
                        showDijalog();
                        break;
                }
                setTitle(naslov);
                masterDrawerLayout.closeDrawer(linearZaDrawer);
            }
        });
        drawerToggle = new ActionBarDrawerToggle(this, masterDrawerLayout, toolbar, R.string.app_name, R.string.app_name) {
            public void onDrawerClosed(View view) {
                invalidateOptionsMenu();
            }

            public void onDrawerOpened(View view) {
                invalidateOptionsMenu();
            }
        };
        masterDrawerLayout.closeDrawer(linearZaDrawer);
    }

    private void dodajNekretninu() {
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.dodaj_nekretninu);
        dialog.setCancelable(false);

        etNaziv = dialog.findViewById(R.id.etNaziv);
        etOpis = dialog.findViewById(R.id.etOpis);
        etAdresa = dialog.findViewById(R.id.etAdresa);
        etBrojTelefona = dialog.findViewById(R.id.etBrojTelefona);
        etKvadratura = dialog.findViewById(R.id.etKvadratura);
        etBrojSoba = dialog.findViewById(R.id.etBrojSoba);
        etCena = dialog.findViewById(R.id.etCena);
        imageViewNekretnina = dialog.findViewById(R.id.imageViewNekretnina);

        btnCancel = dialog.findViewById(R.id.btnCancel);
        btnSave = dialog.findViewById(R.id.btnSave);
        btnDodajSliku = dialog.findViewById(R.id.btnDodajSliku);

        btnDodajSliku.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showSlika();
            }
        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Nekretnina nekretnina = new Nekretnina();

                nekretnina.setNaziv(etNaziv.getText().toString());
                nekretnina.setOpis(etOpis.getText().toString());
                nekretnina.setAdresa(etAdresa.getText().toString());
                nekretnina.setBrTelefona(etBrojTelefona.getText().toString());
                nekretnina.setKvadratura(etKvadratura.getText().toString());
                nekretnina.setBrojSoba(etBrojSoba.getText().toString());
                nekretnina.setCena(etCena.getText().toString());
                nekretnina.setSlika(picturePath);
                listaNekretnina.add(nekretnina);

                try {
                    getDatabaseHelper().getNekretninaDao().create(nekretnina);
                    showNotif("Nekretnina dodata", getApplicationContext());
                    showToast("Nekretnina dodata");
                    refresh();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                dialog.dismiss();
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showNotif("Odustali ste od dodavanja nekretnine", getApplicationContext());
                showToast("Odustali ste od dodavanja nekretnine");
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private void showList() {
        try {
            listaNekretnina = getDatabaseHelper().getNekretninaDao().queryForAll();
            refresh();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        if (listaNekretnina == null) {
            listaNekretnina = new ArrayList<>();
        }

        recyclerView = findViewById(R.id.rvListaNekretnina);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setReverseLayout(true);
        layoutManager.setStackFromEnd(true); //ubacuje na pocetak prvi element a ne na dno
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(new RVAdapter(listaNekretnina, this));
    }

    private void showDijalog() {
        if (aboutDijalog == null) {
            aboutDijalog = new AboutDijalog(this).prepareDialog();
        } else {
            if (aboutDijalog.isShowing()) {
                aboutDijalog.dismiss();
            }
        }
        aboutDijalog.show();
    }


    private void showSettings() {
        Intent intent = new Intent(this, PrefsActivity.class);
        startActivity(intent);
    }

    private void showNotif(String message, Context context) {
        boolean notif = sharedPreferences.getBoolean(NOTIF_SETTINGS, false);
        if (notif) {
            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            NotificationCompat.Builder builder = new NotificationCompat.Builder(context, App.CHANNEL_ID);
            builder.setSmallIcon(R.drawable.ic_action_notif);
            builder.setContentTitle(message);
            builder.setContentText(message);
            notificationManager.notify(NOTIF_ID, builder.build());
        }
    }

    private void showToast(String message) {
        boolean toast = sharedPreferences.getBoolean(TOAST_SETTINGS, false);
        if (toast) Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRVClicked(Nekretnina nekretnina) {
        Intent intent = new Intent(this, DetailsActivity.class);
        intent.putExtra("nekretninaID", nekretnina.getId());
        startActivity(intent);
    }

    public boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                    && checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                return true;
            } else {
                ActivityCompat.requestPermissions(this, new String[] {Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
                return false;
            }
        } else {
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED){
            showSlika();
        }
    }

    private void showSlika() {
        if (isStoragePermissionGranted()) {
            Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(intent, SELECT_PICTURE);
        }
//        Picasso.get()
//                .load("http://becomingminimalist.com/wp-content/uploads/2008/07/post-it-note.jpg?w=300")
//                .resize(30, 30)
//                .into(imageViewRV);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == SELECT_PICTURE && resultCode == RESULT_OK && data != null) {
            Uri selectedImage = data.getData();
            String [] filePathColumn = {MediaStore.Images.Media.DATA};

            if (selectedImage != null) {
                Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                if (cursor != null) {
                    cursor.moveToFirst();

                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    picturePath = cursor.getString(columnIndex);
                    cursor.close();
                    imageViewNekretnina.setImageBitmap(BitmapFactory.decodeFile(picturePath));
                }
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        showList();
    }

    private void refresh() {
        if (recyclerView != null) {
            adapterRV = (RVAdapter) recyclerView.getAdapter();
            if (adapterRV != null) {
                try {
                    listaNekretnina = getDatabaseHelper().getNekretninaDao().queryForAll();
                    adapterRV.notifyDataSetChanged();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public DatabaseHelper getDatabaseHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        }
        return databaseHelper;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        // nakon rada sa bazo podataka potrebno je obavezno
        //osloboditi resurse!
        if (databaseHelper != null) {
            OpenHelperManager.releaseHelper();
            databaseHelper = null;
        }
    }
}
