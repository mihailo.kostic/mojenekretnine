package com.example.mojenekretnine.activities;

import android.app.Dialog;
import android.app.NotificationManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.NotificationCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.preference.PreferenceManager;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mojenekretnine.App;
import com.example.mojenekretnine.DatabaseHelper;
import com.example.mojenekretnine.R;
import com.example.mojenekretnine.model.Nekretnina;
import com.j256.ormlite.android.apptools.OpenHelperManager;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DetailsActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private List<String> drawerItems;
    private ActionBarDrawerToggle drawerToggle;

    private DatabaseHelper databaseHelper;
    private SharedPreferences sharedPreferences;
    private Nekretnina nekretnina;

    public static final int NOTIF_ID = 33;
    public static String TOAST_SETTINGS = "toast_settings_cb";
    public static String NOTIF_SETTINGS = "notif_settings_cb";

    public static final int NOTIFICATION_ID = 5;

    private AlertDialog dijalogBrisanje;
    private AlertDialog dijalogIzmena;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        setupToolbar();
        fillDrawerWithItems();
        setupDrawer();
        showFab();
        showDetails();

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
    }

    private void setupToolbar() {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);
            actionBar.setHomeAsUpIndicator(R.drawable.ic_action_home);
            actionBar.show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_details, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_edit:
                izmeniNekretninu();
                break;
            case R.id.action_delete:
                showDijalogBrisanje();
                break;
        }
        return super.onOptionsItemSelected(item);

    }

    private void fillDrawerWithItems() {
        drawerItems = new ArrayList<>();
        drawerItems.add("Sve nekretnine");
        drawerItems.add("Podesavanja");
    }

    private void setupDrawer() {
        final LinearLayout linearZaDrawer = findViewById(R.id.linear_za_drawerList);
        final DrawerLayout masterDrawerLayout = findViewById(R.id.masterDrawerLayout);
        ListView lvDrawer = findViewById(R.id.lvDrawer);
        lvDrawer.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, drawerItems));
        lvDrawer.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String naslov = "nepoznato";
                switch (position) {
                    case 0:
                        naslov = "Sve nekretnine";
                        showList();
                        break;
                    case 1:
                        naslov = "Podesavanja";
                        showSettings();
                        break;
                }
                setTitle(naslov);
                masterDrawerLayout.closeDrawer(linearZaDrawer);
            }
        });
        drawerToggle = new ActionBarDrawerToggle(this, masterDrawerLayout, toolbar, R.string.app_name, R.string.app_name) {
            public void onDrawerClosed(View view) {
                invalidateOptionsMenu();
            }

            public void onDrawerOpened(View view) {
                invalidateOptionsMenu();
            }
        };
        masterDrawerLayout.closeDrawer(linearZaDrawer);
    }

    private void showSettings() {
        Intent intent = new Intent(this, PrefsActivity.class);
        startActivity(intent);
    }

    private void showList() {
        onBackPressed();
    }

    public DatabaseHelper getDatabaseHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        }
        return databaseHelper;
    }

    private void showFab() {
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean notif = sharedPreferences.getBoolean(NOTIF_SETTINGS, false);
                if (notif) {
                    NotificationManager notificationManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
                    NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext(), App.CHANNEL_ID);
                    builder.setSmallIcon(R.drawable.ic_action_zakazi_pregled);
                    builder.setContentTitle("Zakazivanje razlgedanja");
                    builder.setContentText("Uspesno ste zakazali razgledanje objekta");
                    notificationManager.notify(NOTIFICATION_ID, builder.build());
                } else {
                    Toast.makeText(DetailsActivity.this, "Notifikacije nisu dozvoljene, otvorite podesavanja", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private void showDetails() {
        TextView tvNazivDetalji = findViewById(R.id.tvNazivDetalji);
        ImageView imageViewDetalji = findViewById(R.id.imageviewDetalji);
        TextView etAdresaDetalji = findViewById(R.id.etAdresaDetalji);
        TextView etKvadraturaDetalji = findViewById(R.id.etKvadraturaDetalji);
        TextView etBrojSoba = findViewById(R.id.etBrojSobaDetalji);
        TextView etCenaDetalji = findViewById(R.id.etCenaDetalji);
        TextView etBrojTelefona = findViewById(R.id.etBrojTelefona);
        TextView etOpisDetalji = findViewById(R.id.etOpisDetalji);

        final int nekretninaID = getIntent().getExtras().getInt("nekretninaID");

        try {
            nekretnina = getDatabaseHelper().getNekretninaDao().queryForId(nekretninaID);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        tvNazivDetalji.setText(nekretnina.getNaziv());
        etAdresaDetalji.setText(nekretnina.getAdresa());
        etKvadraturaDetalji.setText(nekretnina.getKvadratura());
        etBrojSoba.setText("" + nekretnina.getBrojSoba());
        etCenaDetalji.setText("" + nekretnina.getCena());
        etBrojTelefona.setText("" + nekretnina.getBrTelefona());
        etOpisDetalji.setText(nekretnina.getOpis());
        if (nekretnina.getSlika() != null) {
            imageViewDetalji.setImageBitmap(BitmapFactory.decodeFile(nekretnina.getSlika()));
        }
    }

    public void klikNaSliku(View view) {
        Dialog dialog = new Dialog(DetailsActivity.this);
        dialog.setContentView(R.layout.image_dijalog);
        dialog.setTitle("Image dialog");

        ImageView imageView = dialog.findViewById(R.id.imageFullSize);
        imageView.setImageBitmap(BitmapFactory.decodeFile(nekretnina.getSlika()));

        dialog.show();
    }

    public void klikNaTelefon(View view) {
        startActivity(new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + nekretnina.getBrTelefona())));
    }

    private void izmeniNekretninu() {
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.dodaj_nekretninu);
        dialog.setCancelable(false);

        final EditText etNaziv = dialog.findViewById(R.id.etNaziv);
        final EditText etOpis = dialog.findViewById(R.id.etOpis);
        final EditText etAdresa = dialog.findViewById(R.id.etAdresa);
        final EditText etBrojTelefona = dialog.findViewById(R.id.etBrojTelefona);
        final EditText etKvadratura = dialog.findViewById(R.id.etKvadratura);
        final EditText etBrojSoba = dialog.findViewById(R.id.etBrojSoba);
        final EditText etCena = dialog.findViewById(R.id.etCena);
        ImageView imageViewDetajli = findViewById(R.id.imageviewDetalji);
        imageViewDetajli.setClickable(true);
        etBrojTelefona.setClickable(true);

        Button btnCancel = dialog.findViewById(R.id.btnCancel);
        Button btnSave = dialog.findViewById(R.id.btnSave);

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (nekretnina != null) {
                    nekretnina.setNaziv(etNaziv.getText().toString());
                    nekretnina.setOpis(etOpis.getText().toString());
                    nekretnina.setAdresa(etAdresa.getText().toString());
                    nekretnina.setBrTelefona(etBrojTelefona.getText().toString());
                    nekretnina.setKvadratura(etKvadratura.getText().toString());
                    nekretnina.setBrojSoba(etBrojSoba.getText().toString());
                    nekretnina.setCena(etCena.getText().toString());

                    try {
                        getDatabaseHelper().getNekretninaDao().update(nekretnina);
                        showToast("Nekretnina izmenjena");
                        showNotif("Nekretnina izmenjena", getApplicationContext());
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                    dialog.dismiss();
                }
            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showNotif("Odustali ste od izmene nekretnine", getApplicationContext());
                showToast("Odustali ste od izmene nekretnine");
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    public class BrisanjeDijalog extends AlertDialog.Builder {

        public BrisanjeDijalog(@NonNull Context context) {
            super(context);
            setTitle("Obrisi nekretninu");
            setMessage("Da li zelite da obrisete nekretninu?");
            setPositiveButton("U redu", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    obrisiNekretninu();
                }
            });
            setNegativeButton("Odustani", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
        }

        public AlertDialog prepareDialog() {
            AlertDialog alertDialog = create();
            alertDialog.setCanceledOnTouchOutside(false);
            return alertDialog;
        }
    }

    private void showDijalogBrisanje() {
        if (dijalogBrisanje == null) {
            dijalogBrisanje = new BrisanjeDijalog(this).prepareDialog();
        } else {
            if (dijalogBrisanje.isShowing()) {
                dijalogBrisanje.dismiss();
            }
        }
        dijalogBrisanje.show();
    }

    private void obrisiNekretninu() {
        if (nekretnina != null) {
            try {
                getDatabaseHelper().getNekretninaDao().delete(nekretnina);
                showToast("Nekretnina obrisana");
                showNotif("Nekretnina obrisana", getApplicationContext());
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        onBackPressed();
    }

    private void showToast(String message) {
        boolean toast = sharedPreferences.getBoolean(TOAST_SETTINGS, false);
        if (toast) Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    private void showNotif(String message, Context context) {
        boolean notif = sharedPreferences.getBoolean(NOTIF_SETTINGS, false);
        if (notif) {
            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            NotificationCompat.Builder builder = new NotificationCompat.Builder(context, App.CHANNEL_ID);
            builder.setSmallIcon(R.drawable.ic_action_notif);
            builder.setContentTitle(message);
            builder.setContentText(message);
            notificationManager.notify(NOTIF_ID, builder.build());
        }
    }
}
